//
//  PhotoTests.swift
//  MyPicFarmTests
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import XCTest
@testable import MyPicFarm

class PhotoTests: XCTestCase {
    
    func test_NilImageURL() {
        let nilFarmPhoto = Photo(id: "23451156376",
                                 owner: "28017113@N08",
                                 secret: "8983a8ebc7",
                                 server: "578",
                                 farm: nil,
                                 title: "Test Title",
                                 isPublic: 1,
                                 isFriend: 0,
                                 isFamily: 0)
        
        let nilServerPhoto = Photo(id: "23451156376",
                                 owner: "28017113@N08",
                                 secret: "8983a8ebc7",
                                 server: nil,
                                 farm: 1,
                                 title: "Test Title",
                                 isPublic: 1,
                                 isFriend: 0,
                                 isFamily: 0)
        
        let nilIdPhoto = Photo(id: nil,
                                 owner: "28017113@N08",
                                 secret: "8983a8ebc7",
                                 server: "578",
                                 farm: 1,
                                 title: "Test Title",
                                 isPublic: 1,
                                 isFriend: 0,
                                 isFamily: 0)
        
        let nilSecretPhoto = Photo(id: "23451156376",
                                 owner: "28017113@N08",
                                 secret: "8983a8ebc7",
                                 server: nil,
                                 farm: 1,
                                 title: "Test Title",
                                 isPublic: 1,
                                 isFriend: 0,
                                 isFamily: 0)
        
        let nilImagePhoto = Photo(id: nil,
                                 owner: "28017113@N08",
                                 secret: nil,
                                 server: nil,
                                 farm: nil,
                                 title: "Test Title",
                                 isPublic: 1,
                                 isFriend: 0,
                                 isFamily: 0)
        
        XCTAssertNil(nilFarmPhoto.imageURL, "Image URL should be nil when Farm is nil")
        XCTAssertNil(nilServerPhoto.imageURL, "Image URL should be nil when Server is nil")
        XCTAssertNil(nilIdPhoto.imageURL, "Image URL should be nil when Id is nil")
        XCTAssertNil(nilSecretPhoto.imageURL, "Image URL should be nil when Secret is nil")
        XCTAssertNil(nilImagePhoto.imageURL, "Image URL should be nil when required fields are nil")
    }
    
    func test_ValidImageURL() {
        let photo = Photo(id: "23451156376",
                          owner: "28017113@N08",
                          secret: "8983a8ebc7",
                          server: "578",
                          farm: 1,
                          title: "Test Title",
                          isPublic: 1,
                          isFriend: 0,
                          isFamily: 0)
        let expectedPhotoImageURL = URL(string: "http://farm1.static.flickr.com/578/23451156376_8983a8ebc7.jpg")
        
        let customPhoto = Photo(id: "99",
                                owner: nil,
                                secret: "9999a9ebc9",
                                server: "999",
                                farm: 9,
                                title: nil,
                                isPublic: nil,
                                isFriend: nil,
                                isFamily: nil)
        let expectedCustomPhotoImageURL = URL(string: "http://farm9.static.flickr.com/999/99_9999a9ebc9.jpg")
        
        XCTAssertNotNil(photo.imageURL, "Image URL should not be nil.")
        XCTAssertEqual(photo.imageURL, expectedPhotoImageURL, "Photo image URL should match expected URL.")
        
        XCTAssertNotNil(customPhoto.imageURL, "Image URL should not be nil.")
        XCTAssertEqual(customPhoto.imageURL, expectedCustomPhotoImageURL, "Custom Photo image URL should match expected URL.")
    }
}
