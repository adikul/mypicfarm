//
//  ImageViewExtensionTests.swift
//  MyPicFarmTests
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import XCTest
@testable import MyPicFarm

class ImageViewExtensionTests: XCTestCase {

    func test_imageDownload() {
        let viewModel = ViewModel()
        viewModel.configure(delegate: nil)
        let imageView = UIImageView()
        let defaultImage = UIImage(named: "no-image")
        let testImageURL = URL(string: "https://farm1.static.flickr.com/578/23451156376_8983a8ebc7.jpg")!
        
        imageView.downloaded(from: testImageURL)
        XCTAssertEqual(imageView.image, defaultImage, "Image view should show default image before actual image is downloaded.")
        
        let exp = expectation(description: "Test after Download")
        let result = XCTWaiter.wait(for: [exp], timeout: 3.0)
        if result == XCTWaiter.Result.timedOut {
            XCTAssertNotEqual(imageView.image, defaultImage, "Image view should show actual image after downloaded")
        } else {
            XCTFail("Something went wrong.")
        }
        
        let imageFromCache = imageCache.object(forKey: testImageURL as AnyObject) as? UIImage
        XCTAssertNotNil(imageFromCache, "Once image is downloaded, Image cache should provide it.")
    }
    
    func test_imageDownloadFail() {
        let viewModel = ViewModel()
        viewModel.configure(delegate: nil)
        let imageView = UIImageView()
        let defaultImage = UIImage(named: "no-image")
        let testImageURL = URL(string: "https://farm.static.flickr.com/578/23451156376_8983a8ebc7.jpg")!
        
        imageView.downloaded(from: testImageURL)
        XCTAssertEqual(imageView.image, defaultImage, "Image view should show default image before actual image is downloaded.")
        
        let exp = expectation(description: "Test after Download")
        let result = XCTWaiter.wait(for: [exp], timeout: 3.0)
        if result == XCTWaiter.Result.timedOut {
            XCTAssertEqual(imageView.image, defaultImage, "Image download should fail and imageview should show default image")
        } else {
            XCTFail("Something went wrong.")
        }
    }
}
