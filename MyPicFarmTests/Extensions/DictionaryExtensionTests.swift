//
//  DictionaryExtensionTests.swift
//  MyPicFarmTests
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import XCTest
@testable import MyPicFarm

class DictionaryExtensionTests: XCTestCase {
    
    func test_blankParamString() {
        let params: Parameters = [:]
        
        XCTAssertEqual(params.paramsString(), "", "Param String value should be a blank string for blank Dictionary.")
    }
    
    func test_nilParams() {
        let params: Parameters? = nil
        
        XCTAssertNil(params?.paramsString(), "Param string value should be nil when dictionary is nil.")
    }
    
    func test_validParamString() {
        let params: Parameters = [
            "key1": "value1",
            "key2": "value2"
        ]
        let expectedParamString = "key1=value1&key2=value2"
        
        XCTAssertEqual(params.paramsString(), expectedParamString, "Param string value should match with expected value.")
    }
    
    func test_validParamStringForSingleKeyValue() {
        let params: Parameters = [
            "key1": "value1"
        ]
        let expectedParamString = "key1=value1"
        
        XCTAssertEqual(params.paramsString(), expectedParamString, "Param string value should match with expected value.")
    }
}
