//
//  ViewController.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import UIKit

class ViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    //MARK:- Properties
    
    //MARK:- Constants
    private let viewModel = ViewModel()
    private let footerView = UIActivityIndicatorView(style: .whiteLarge)
    
    let columnLayout = ColumnFlowLayout(cellsPerRow: 3,
                                        minimumInteritemSpacing: 10,
                                        minimumLineSpacing: 10,
                                        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
    
    //MARK:- View Controller Lifecycle Events
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    //MARK:- IBActions
    
    //MARK:- Methods
    private func configureView() {
        viewModel.configure(delegate: self)
        configureSearchBar()
        configureCollectionView()
        footerView.color = .black
    }
    
    private func configureSearchBar() {
        searchBar.delegate = self
        navigationItem.titleView = searchBar
    }
    
    private func configureCollectionView() {
        photosCollectionView.register(UINib(nibName: Constants.CollectionViewCell.photo, bundle: nil),
                                      forCellWithReuseIdentifier: Constants.CollectionViewCell.photo)
        photosCollectionView.register(CollectionViewFooterView.self,
                                      forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                                      withReuseIdentifier: Constants.CollectionViewCell.activity)
        columnLayout.footerReferenceSize = CGSize(width: photosCollectionView.bounds.width, height: 50)
        
        photosCollectionView.collectionViewLayout = columnLayout
        photosCollectionView.contentInsetAdjustmentBehavior = .always
        
        photosCollectionView.delegate = self
        photosCollectionView.dataSource = self
    }
    
    private func searchPhotos(text: String?, refresh: Bool) {
        footerView.startAnimating()
        viewModel.searchPhotos(searchString: text, refresh: refresh)
    }
}

extension ViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchPhotos(text: searchBar.text, refresh: true)
        searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.endEditing(true)
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getNumberOfPhotos()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CollectionViewCell.photo, for: indexPath) as! PhotoCollectionViewCell
        cell.configureCell(withPhotoURL: viewModel.getPhotoURL(ForIndex: indexPath.row))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if viewModel.shouldFetchMore(index: indexPath.row) {
            searchPhotos(text: viewModel.searchText, refresh: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                                 viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                         withReuseIdentifier: Constants.CollectionViewCell.activity,
                                                                         for: indexPath)
            footer.addSubview(footerView)
            footerView.frame = CGRect(x: 0, y: 0, width: collectionView.bounds.width, height: 50)
            return footer
        }
        return UICollectionReusableView()
    }
}

extension ViewController: ViewModelDelegate {
    func showAlert(errorCode: Int, errorMessage: String) {
        Utilities.showAlert(withTitle: "Error \(errorCode)", message: errorMessage)
    }
    
    func loadData() {
        footerView.stopAnimating()
        photosCollectionView.reloadData()
    }
    
    func clearData() {
        photosCollectionView.reloadData()
    }
}

