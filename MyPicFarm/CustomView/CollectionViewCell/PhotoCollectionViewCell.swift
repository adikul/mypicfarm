//
//  PhotoCollectionViewCell.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    func configureCell(withPhotoURL imageURL: URL?) {
        if let url = imageURL {
            imageView.downloaded(from: url)
        }
    }
}
