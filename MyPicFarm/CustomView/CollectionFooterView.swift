//
//  CollectionFooterView.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import UIKit

class CollectionViewFooterView: UICollectionReusableView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
