//
//  ViewModel.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation

protocol ViewModelDelegate {
    func clearData()
    func loadData()
    func showAlert(errorCode: Int, errorMessage: String)
}

let imageCache = NSCache<AnyObject, AnyObject>()

class ViewModel {
    private let repository: Repository
    
    private var delegate: ViewModelDelegate?
    private var currentPage: Int?
    private var totalPages: Int?
    private var photos = [Photo]()
    private(set) var searchText = ""
    
    init() {
        repository = Repository(client: APIClient())
    }
    
    func configure(delegate: ViewModelDelegate?) {
        self.delegate = delegate
        imageCache.totalCostLimit = 1024 * 1024 * 200
    }
    
    func searchPhotos(searchString: String?, refresh: Bool = false) {
        guard let searchString = searchString else { return }
        if refresh {
            imageCache.removeAllObjects()
            photos.removeAll()
            searchText = ""
            delegate?.clearData()
        }
        searchText = searchString
        let page = refresh ? 1 : ((currentPage ?? 0) + 1)
        repository.searchPhotos(withSearchQuery: searchString, page: page, perPage: Constants.AppConstants.photosPerPage) {[weak self] result in
            switch result {
            case .success(let photoResponse):
                if photoResponse.status == "ok" {
                    self?.currentPage = photoResponse.photosPage?.page
                    self?.totalPages = photoResponse.photosPage?.totalPages
                    if let list = photoResponse.photosPage?.photoList {
                        self?.photos.append(contentsOf: list)
                    }
                    DispatchQueue.main.async {
                        self?.delegate?.loadData()
                    }
                } else {
                    if let errorCode = photoResponse.errorCode,
                       let errorMessage = photoResponse.errorMessage {
                        DispatchQueue.main.async {
                            self?.delegate?.showAlert(errorCode: errorCode, errorMessage: errorMessage)
                        }
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getNumberOfPhotos() -> Int {
        return photos.count
    }
    
    func getPhotoURL(ForIndex index: Int) -> URL? {
        return photos[index].imageURL
    }
    
    func shouldFetchMore(index: Int) -> Bool {
        if let currentPage = currentPage,
           let totalPages = totalPages,
           currentPage < totalPages && index == photos.count - 1 {
            return true
        }
        return false
    }
}
