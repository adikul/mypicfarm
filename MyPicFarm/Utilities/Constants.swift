//
//  Constants.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation
import UIKit

typealias Parameters = [String: Any]
typealias AlertAction = ((UIAlertAction) -> Void)?

class Constants {
    enum AppConstants {
        static let photosPerPage = 30
    }
    
    enum CollectionViewCell {
        static let photo = "PhotoCollectionViewCell"
        static let activity = "CollectionActivityCell"
    }
}
