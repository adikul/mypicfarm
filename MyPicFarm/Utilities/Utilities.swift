//
//  Utilities.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import UIKit

class Utilities {
    static func showAlert(withTitle title: String,
                          message: String,
                          buttonTitle: String = "OK",
                          onViewController vc: UIViewController? = nil, withHandler: AlertAction = nil) {
        var topVC = vc
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController {
            topVC = rootVC
            if let vc = rootVC.presentedViewController {
                topVC = vc
            }
        }
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: buttonTitle, style: .cancel, handler: withHandler)
        alertController.addAction(okAction)
        topVC?.present(alertController, animated: true, completion: nil)
    }
}
