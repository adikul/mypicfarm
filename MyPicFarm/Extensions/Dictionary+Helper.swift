//
//  Dictionary+Helper.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation

extension Dictionary {
    func paramsString() -> String {
        var paramsString = [String]()
        for (key, value) in self {
            guard let stringKey = key as? String,
                  let stringValue = value as? String else {
                return ""
            }
            paramsString.append(stringKey + "=" + "\(stringValue)")
        }
        return (paramsString.isEmpty ? "" : paramsString.joined(separator: "&"))
    }
}
