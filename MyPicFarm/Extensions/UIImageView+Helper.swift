//
//  UIImageView+Helper.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import UIKit

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFill) {
        let defaultImage = UIImage(named: "no-image")
        contentMode = mode
        image = defaultImage
        if let imageFromCache = imageCache.object(forKey: url as AnyObject) as? UIImage {
            image = imageFromCache
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let httpURLResponse = response as? HTTPURLResponse,
                httpURLResponse.statusCode == 200,
                let data = data,
                error == nil,
                let image = UIImage(data: data)
                else {
                    DispatchQueue.main.async() { [weak self] in
                        self?.image = defaultImage
                    }
                    return
                }
            imageCache.setObject(image, forKey: url as AnyObject)
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
}
