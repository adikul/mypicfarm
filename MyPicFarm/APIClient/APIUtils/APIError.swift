//
//  APIError.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation

enum APIError: Error {
    case noData
}
