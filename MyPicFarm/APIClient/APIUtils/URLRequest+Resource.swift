//
//  URLRequest+Resource.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation

extension URLRequest {
    init(_ resource: Resource) {
        self.init(url: resource.url)
        self.httpMethod = resource.method.rawValue
    }
}
