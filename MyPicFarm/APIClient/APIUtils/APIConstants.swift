//
//  APIConstants.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation

enum APIConstants {
    static let baseURL = "https://api.flickr.com/services/rest/"
    static let apiKey = "2932ade8b209152a7cbb49b631c4f9b6"
    static let responseFormat = "json"
    static let mediaType = "photos"
}

enum MethodType: String {
    case get = "GET"
}
