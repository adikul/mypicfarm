//
//  GenericResult.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
}
