//
//  APIMethod.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation

enum APIMethod {
    static let searchPhotos = "flickr.photos.search"
}
