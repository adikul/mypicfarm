//
//  Resource.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation

struct Resource {
    let url: URL
    let method: MethodType
}
