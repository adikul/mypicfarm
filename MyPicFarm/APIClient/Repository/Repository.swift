//
//  Repository.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation

class Repository {
    let client: APIClient!
    
    init(client: APIClient) {
        self.client = client
    }
}
