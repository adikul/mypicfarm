//
//  Repository+PhotoSearch.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation

extension Repository {
    
    func searchPhotos(withSearchQuery text: String, page: Int, perPage: Int, completion: @escaping ((Result<PhotoResponse>) -> Void)) {
        var baseURL = APIConstants.baseURL
        let queryParams: Parameters = [
            "method": APIMethod.searchPhotos,
            "api_key": APIConstants.apiKey,
            "%20format": APIConstants.responseFormat,
            "nojsoncallback": "\(1)",
            "safe_search": "\(1)",
            "media": APIConstants.mediaType,
            "page": "\(page)",
            "per_page": "\(perPage)",
            "text": text
        ]
        baseURL.append("?\(queryParams.paramsString())")
        guard let url = URL(string: baseURL) else { return }
        let searchPhotosResource = Resource(url: url, method: .get)
        
        client.load(searchPhotosResource) { result in
            switch result {
            case .success(let data):
                do {
                    let response = try JSONDecoder().decode(PhotoResponse.self, from: data)
                    completion(.success(response))
                } catch(let err) {
                    completion(.failure(err))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
