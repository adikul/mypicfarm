//
//  PhotosPage.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation

struct PhotosPage: Decodable {
    let page: Int
    let totalPages: Int
    let photosPerPage: Int
    let totalPhotos: Int
    let photoList: [Photo]
}

extension PhotosPage {
    enum CodingKeys: String, CodingKey {
        case page = "page"
        case totalPages = "pages"
        case photosPerPage = "perpage"
        case totalPhotos = "total"
        case photoList = "photo"
    }
}
