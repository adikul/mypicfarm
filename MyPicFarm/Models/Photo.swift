//
//  Photo.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation

struct Photo: Decodable {
    let id: String?
    let owner: String?
    let secret: String?
    let server: String?
    let farm: Int?
    let title: String?
    let isPublic: Int?
    let isFriend: Int?
    let isFamily: Int?
    
    var imageURL: URL? {
        guard let farm = farm,
              let server = server,
              let id = id,
              let secret = secret else { return nil }
        
        let urlString = "http://farm\(farm).static.flickr.com/\(server)/\(id)_\(secret).jpg"
        return URL(string: urlString)
    }
}

extension Photo {
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case owner = "owner"
        case secret = "secret"
        case server = "server"
        case farm = "farm"
        case title = "title"
        case isPublic = "ispublic"
        case isFriend = "isfriend"
        case isFamily = "isfamily"
    }
}
