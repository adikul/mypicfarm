//
//  PhotoResponse.swift
//  MyPicFarm
//
//  Created by Aditya Kulkarni on 04/07/21.
//

import Foundation

struct PhotoResponse: Decodable {
    let photosPage: PhotosPage?
    let status: String
    let errorCode: Int?
    let errorMessage: String?
}

extension PhotoResponse {
    enum CodingKeys: String, CodingKey {
        case photosPage = "photos"
        case status = "stat"
        case errorCode = "code"
        case errorMessage = "message"
    }
}
